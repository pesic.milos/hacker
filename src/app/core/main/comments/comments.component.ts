import { Component, OnInit, Input } from '@angular/core';
import { HackerService } from 'src/app/service/hacker.service';
import { Item } from '../../modal/item';
import { Comment } from '../../modal/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() komentar: [];
  item: Item;
  formattedTime: any;

  

  constructor(private service: HackerService) { }

  ngOnInit(): void {
    this.service.getItem(+this.komentar).subscribe(el=>{
      this.item = el;
      // console.log(this.item);

      this.timer(this.item?.time);

      // let time = this.item?.time;
      // let date = new Date(time * 1000);
      // let hours = date.getHours();
      // let minutes = "0" + date.getMinutes();
      // let seconds = "0" + date.getSeconds();

      // this.formattedTime = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
      // console.log(this.formattedTime);
      // console.log(time);
    })

    

    // console.log(this.komentar);
  }

  public timer(unitime){
      let time = unitime;
      let date = new Date(time * 1000);
      let hours = date.getHours();
      let minutes = "0" + date.getMinutes();
      let seconds = "0" + date.getSeconds();

      this.formattedTime = hours + ":" + minutes.substr(-2) + ":" + seconds.substr(-2);
  }

}
