import { Component, OnInit } from '@angular/core';
import { HackerService } from 'src/app/service/hacker.service';
import { Item } from '../modal/item';
import { ItemList } from '../modal/itemList';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  itemList: ItemList;
  any: any;
  // item: Item;
  
  pageSize = 10;
  

  constructor(private service: HackerService) { }

  ngOnInit(): void {
    // this.service.getAll().subscribe(el=> {
    //   this.any = el;
      // niz id-a
      // console.log(this.any);
    // });

    // ovako uraditi
    this.refresh();

  }

  addMore(){
    this.pageSize = this.pageSize + 10;
    this.refresh();
  }

  refresh(){
    this.service.getAll2().subscribe(el => {
      this.itemList = el;
      this.any = this.itemList?.results.slice(0 ,this.pageSize);
      // console.log(this.any);
    });
  }
}


