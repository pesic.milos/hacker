import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../modal/item';
import { HackerService } from 'src/app/service/hacker.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Input() ids: [];
  item: Item;

  show = false;

  constructor(private service: HackerService) { }

  ngOnInit(): void {
    
    this.service.getItem(+this.ids).subscribe(el=>{
      this.item = el;
      // this.item.counter = this.ids.indexOf("1234");
    });
  }
}
