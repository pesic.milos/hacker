import { Item } from './item';

export class ItemList {
    results: []

    constructor(obj?: any){
        this.results = obj || [];
    }
}