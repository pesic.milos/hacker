export class Comment {
    by: string;
    id: number;
    text: string;
    time: number;
    type: string;

    constructor(obj?:any){
        this.by = obj || '';
        this.id = obj || null;
        this.text = obj || null;
        this.time = obj || null;
        this.type = obj || '';
    }

}