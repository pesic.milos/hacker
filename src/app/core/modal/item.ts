export class Item {
    counter: number;
    _id: number;
    // deleted: boolean;
    type: string;
    by: string;
    time: number;
    text: string;
    // dead: boolean;
    parent: number;
    // poll: string;
    kids: [];
    url: string;
    score: number;
    title: string;
    // parts: [];
    // descendants: number;

    constructor(obj?: any){
        this._id = obj && obj._id || null;
        // this.deleted = obj && obj.deleted || null;
        this.type = obj && obj.type || '';
        this.by = obj && obj.by || '';
        this.time = obj && obj.time || null;
        this.text = obj && obj.text || '';
        // this.dead = obj && obj.dead || null;
        this.parent = obj && obj.parent || null;
        // this.poll = obj && obj.poll || '';
        this.kids = obj && obj.kids || [];
        this.url = obj && obj.url || '';
        this.score = obj && obj.score || null;
        this.title = obj && obj.title || '';
        // this.parts = obj && obj.parts || [];
        // this.descendants = obj && obj.descendants || null;
    }
}