import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './core/main/main.component';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { DetailComponent } from './core/main/detail/detail.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CommentsComponent } from './core/main/comments/comments.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DetailComponent,
    NavbarComponent,
    CommentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // NgModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
