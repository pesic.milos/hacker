import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './core/main/main.component';
import { CommentsComponent } from './core/main/comments/comments.component';


const routes: Routes = [
  // {path: 'home', component: MainComponent},
  {path: 'stories', component: MainComponent},
  // {path: 'stories/comments', component: CommentsComponent},
  // {path: '', redirectTo:'/stories', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
