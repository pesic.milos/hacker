import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Item } from '../core/modal/item';
import { map, catchError } from 'rxjs/operators';
import { ItemList } from '../core/modal/itemList';
import { Comment } from '../core/modal/comment';

// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';


// const baseUrl = "https://hacker-news.firebaseio.com/v0/";
const topstoriesUrl = "https://hacker-news.firebaseio.com/v0/topstories.json";
const storyUrl = "https://hacker-news.firebaseio.com/v0/item/";


@Injectable({
  providedIn: 'root'
})
export class HackerService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<[]>{
    return this.http.get<[]>(topstoriesUrl);
  }

  // ovako uraditi
  getAll2(): Observable<ItemList>{
    return this.http.get<ItemList>(topstoriesUrl).pipe(map(el=>{
      return new ItemList(el);
    }))
  }

  // private errorHandler(errorResponse: HttpErrorResponse){
  //   return Observable.throw(errorResponse.message || "Server Error");
    // if (errorResponse.error instanceof ErrorEvent){
    //   console.error("Client Side Error: ", errorResponse.error.message);
    // } else {
    //   console.error("server Side Error: ", errorResponse);
    // }
    // return new ErrorObservable
  // }

  getItem(id:number): Observable<Item>{
    return this.http.get<Item>(storyUrl + id + ".json").pipe(map(el=>{
      return new Item(el);
    }));
  };

  // getComment(id:number): Observable<Comment>{
  //   return this.http.get<Comment>(storyUrl + id + ".json").pipe(map(el=>{
  //     return new Comment(el);
  //   }));
  // };

}
